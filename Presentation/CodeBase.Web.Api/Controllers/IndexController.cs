﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeBase.Core;
using CodeBase.Core.Filters;
using CodeBase.Core.Localization.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace CodeBase.Web.Api.Controllers
{
    [Route("/")]
    public class IndexController : ControllerBase
    {
        private readonly IStringLocalizer<Message> _stringLocalizer;
        private readonly IServiceProvider _serviceProvider;

        public IndexController(IStringLocalizer<Message> stringLocalizer, IServiceProvider serviceProvider)
        {
            _stringLocalizer = stringLocalizer;
            _serviceProvider = serviceProvider;
        }

        [HttpGet]
        [ApiWrapper(Skip = true)]
        [AllowAnonymous]
        public string Get()
        {
            return _stringLocalizer[Message.Hello];
        }

        [HttpGet, Route("/services")]
        [AllowAnonymous]
        public List<string> GetServices()
        {
            List<string> services = new List<string>();
            var col = EngineContext.Current.ServiceCollection.Where(prop => prop.ServiceType.FullName.Contains("CodeBase")).ToList();
            foreach (var service in col)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"{service.ServiceType.Name}, ");
                sb.Append($"{service.Lifetime}, ");
                sb.Append($"{service.ImplementationType?.FullName}.");

                services.Add(sb.ToString());
            }

            return services;
        }
    }
}
