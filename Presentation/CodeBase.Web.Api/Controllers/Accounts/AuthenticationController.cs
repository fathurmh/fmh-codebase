﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CodeBase.Core;
using CodeBase.Core.Abstractions;
using CodeBase.Core.Extensions;
using CodeBase.Services.Abstractions;
using CodeBase.Web.Api.Models.Accounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CodeBase.Web.Api.Controllers.Accounts
{
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("token")]
        [AllowAnonymous]
        public async Task<object> Token(LoginRequest request)
        {
            return await _authenticationService.Login(request.Username, request.Password);
        }

        [HttpGet("info")]
        public async Task<object> Info()
        {
            var identity = EngineContext.Current.Principal.Identity;
            var info = new { identity.Name, Roles = identity.GetRoleTypeClaimValue() };
            return await Task.FromResult(info);
        }
    }
}
