﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeBase.Core;
using CodeBase.Core.Filters;
using CodeBase.Core.Configuration;
using CodeBase.Repositories.DbManager;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CodeBase.Web.Api.Extensions
{
    /// <summary>
    /// Represents extensions of IServiceCollection
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="configuration">IConfiguration</param>
        public static void ConfigureApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddStartupConfig<ConnectionStrings>(configuration.GetSection(nameof(ConnectionStrings)));
            services.AddStartupConfig<CoreConfig>(configuration.GetSection(nameof(CoreConfig)));
            services.AddStartupConfig<JwtAuthentication>(configuration.GetSection(nameof(CoreConfig)).GetSection(nameof(JwtAuthentication)), reloadOnChange: true);
            services.AddStartupConfig<CultureInfo>(configuration.GetSection(nameof(CoreConfig)).GetSection(nameof(CultureInfo)), reloadOnChange: true);

            var engine = EngineContext.Create(services);
            engine.ConfigureServices(configuration)
                .ConfigureDbContext()
                .ConfigureAuthentication();
        }

        /// <summary>
        /// Configure database context services
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        public static IServiceCollection ConfigureDbContext(this IServiceCollection services)
        {
            var connection = EngineContext.Current.Resolve<ConnectionStrings>();
            services.AddDbContext<CoreDbContext>(options =>
            {
                options.UseSqlServer(connection[ConnectionStrings.Default], builder => builder.MigrationsAssembly(typeof(Startup).Namespace));
            });

            return services;
        }

        /// <summary>
        /// Configure authentication
        /// </summary>
        /// <param name="services">This services</param>
        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(auth =>
                {
                    auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwt =>
                {
                    var config = EngineContext.Current.Resolve<JwtAuthentication>();
                    var key = Encoding.ASCII.GetBytes(config.Key);
                    jwt.RequireHttpsMetadata = false;
                    jwt.SaveToken = true;
                    jwt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = !string.IsNullOrWhiteSpace(config.Issuer),
                        ValidateAudience = !string.IsNullOrWhiteSpace(config.Audience),
                        ValidIssuer = config.Issuer,
                        ValidAudience = config.Audience
                    };
                });

            return services;
        }

        /// <summary>
        /// Create, bind and register as service the specified configuration parameters 
        /// </summary>
        /// <typeparam name="TConfig">Configuration parameters</typeparam>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Set of key/value application configuration properties</param>
        /// <param name="reloadOnChange">Whether the configuration should be reloaded if file changes (default is false)</param>
        /// <returns>Instance of configuration parameters</returns>
        public static TConfig AddStartupConfig<TConfig>(this IServiceCollection services, IConfiguration configuration, bool reloadOnChange = false) where TConfig : class, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            //create instance of config
            var config = new TConfig();

            if (reloadOnChange)
            {
                //configure it to the appropriate section of configuration
                services.Configure<TConfig>(configuration);

                //and register it as a scoped options snapshot service
                services.AddScoped(option => option.GetService<IOptionsSnapshot<TConfig>>().Value);
            }
            else
            {
                //bind it to the appropriate section of configuration
                configuration.Bind(config);

                //and register it as a singleton service
                services.AddSingleton(config);
            }

            return config;
        }
    }
}
