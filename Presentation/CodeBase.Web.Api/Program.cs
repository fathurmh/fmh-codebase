﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CodeBase.Core;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CodeBase.Web.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var hosting = CreateWebHostBuilder(args).Build();
            await EngineContext.Current.PostConfigureAsync(hosting);
            await hosting.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
