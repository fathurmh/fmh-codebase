﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeBase.Web.Api.Migrations
{
    public partial class SecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "UserRole",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedTime",
                table: "UserRole",
                type: "datetime",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "User",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedTime",
                table: "User",
                type: "datetime",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Role",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedTime",
                table: "Role",
                type: "datetime",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Department",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedTime",
                table: "Department",
                type: "datetime",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedTime",
                table: "UserRole");

            migrationBuilder.DropColumn(
                name: "DeletedTime",
                table: "User");

            migrationBuilder.DropColumn(
                name: "DeletedTime",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "DeletedTime",
                table: "Department");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "UserRole",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "User",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Role",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Department",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");
        }
    }
}
