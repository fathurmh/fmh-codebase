﻿using CodeBase.Core.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace CodeBase.Core.Infrastructures.Abstractions
{
    /// <summary>
    /// Represents dependency registrar
    /// </summary>
    public interface IDependencyRegistrar : ISingletonService
    {
    }
}
