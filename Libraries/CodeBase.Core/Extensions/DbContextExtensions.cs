using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CodeBase.Core.Data.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.DependencyInjection;

namespace CodeBase.Core.Extensions
{
    /// <summary>
    /// Represents a db context extensions class
    /// </summary>
    public static class DbContextExtensions
    {
        /// <summary>
        /// Migrate database context that inherits from DbContext or implements from IDbContext
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public static async Task DatabaseMigrateAsync(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var contextTypes = Assembly.GetExecutingAssembly().GetTypes()
                    .Where(type => (type.BaseType == typeof(DbContext) || type.GetInterfaces().Contains(typeof(IDbContext))));
                
                foreach (var contextType in contextTypes)
                {
                    try
                    {
                        var context = (DbContext)scope.ServiceProvider.GetRequiredService(contextType);
                        await context.Database.MigrateAsync();
                    }
                    catch
                    {
                        // Logging here soon
                    }
                }
            }
        }
        
        /// <summary>
        /// Audit db context
        /// </summary>
        /// <param name="context">DbContext</param>
        public static void Audit(this IDbContext context)
        {
            string currentUsername = EngineContext.Current.Principal.Identity.Name;
            IEnumerable<EntityEntry> auditEntries = context.GetCurrentEntries();

            try
            {
                foreach (var auditEntry in auditEntries)
                {
                    DateTime dateTimeNow = DateTime.Now;

                    switch(auditEntry.State)
                    {
                        case EntityState.Added:
                        {
                            auditEntry.CurrentValues[nameof(IAuditable.CreatedBy)] = currentUsername;
                            auditEntry.CurrentValues[nameof(IAuditable.CreatedTime)] = dateTimeNow;
                        }
                        break;
                        case EntityState.Modified:
                        {
                            auditEntry.WriteUserModifier(currentUsername, dateTimeNow);
                        }
                        break;
                        case EntityState.Deleted:
                        {
                            if (typeof(ISoftDeletable).IsAssignableFrom(auditEntry.Entity.GetType()))
                            {
                                var deletedKeyProperty = auditEntry.OriginalValues.Properties.Where(prop => prop.Name.Equals(nameof(ISoftDeletable.IsDeleted))).SingleOrDefault();

                                auditEntry.CurrentValues[deletedKeyProperty] = true;
                                auditEntry.CurrentValues[nameof(ISoftDeletable.DeletedTime)] = dateTimeNow;
                                auditEntry.WriteUserModifier(currentUsername, dateTimeNow);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Rollback of entity changes and return full error message asynchronously
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>Error message</returns>
        public static string RollbackEntityChanges(this IDbContext context, DbUpdateException exception)
        {
            context.RollbackEntity();
            context.SaveChanges();
            return exception.ToString();
        }

        /// <summary>
        /// Rollback of entity changes and return full error message
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>Error message</returns>
        public static async Task<string> RollbackEntityChangesAsync(this IDbContext context, DbUpdateException exception)
        {
            context.RollbackEntity();
            await context.SaveChangesAsync();
            return exception.ToString();
        }
        
        /// <summary>
        /// Get current entity entries
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<EntityEntry> GetCurrentEntries(this IDbContext context)
        {
            return context.ChangeTracker.Entries()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified || entry.State == EntityState.Deleted);
        }
        
        private static EntityEntry WriteUserModifier(this EntityEntry entry, string currentUsername, DateTime now)
        {
            Type auditEntryType = entry.Entity.GetType();
            if (typeof(IAuditable).IsAssignableFrom(auditEntryType))
            {
                entry.CurrentValues[nameof(IAuditable.ModifiedBy)] = currentUsername;
                entry.CurrentValues[nameof(IAuditable.ModifiedTime)] = now;
                entry.State = EntityState.Modified;
            }
            return entry;
        }

        private static void RollbackEntity(this IDbContext context)
        {
            var entries = context.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified).ToList();

            entries.ForEach(entry =>
            {
                try
                {
                    entry.State = EntityState.Unchanged;
                }
                catch (InvalidOperationException ex)
                {
                    throw ex;
                }
            });
        }
    }
}
