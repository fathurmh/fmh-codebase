﻿namespace CodeBase.Core.Localization.Resources
{
    /// <summary>
    /// Represents an api response message
    /// </summary>
    public sealed class ApiResponseMessage
    {
        /// <summary>
        /// Gets the success api response message
        /// </summary>
        public const string Success = "Success";

        /// <summary>
        /// Gets the exception api response message
        /// </summary>
        public const string Exception = "Exception";

        /// <summary>
        /// Gets the unauthorized api response message
        /// </summary>
        public const string Unauthorized = "Unauthorized";

        /// <summary>
        /// Gets the validation error api response message
        /// </summary>
        public const string ValidationError = "ValidationError";

        /// <summary>
        /// Gets the failure api response message
        /// </summary>
        public const string Failure = "Failure";

        /// <summary>
        /// Gets the not found api response message
        /// </summary>
        public const string NotFound = "NotFound";

        /// <summary>
        /// Gets the contact support api response message
        /// </summary>
        public const string ContactSupport = "ContactSupport";

        /// <summary>
        /// Gets the validation exception api response message
        /// </summary>
        public const string ValidationException = "ValidationException";

        /// <summary>
        /// Gets the unhandled exception api response message
        /// </summary>
        public const string Unhandled = "Unhandled";
    }
}
