﻿namespace CodeBase.Core.Configuration
{
    /// <summary>
    /// Represents startup core configuration parameters
    /// </summary>
    public class CoreConfig
    {
        /// <summary>
        /// Gets or sets the core connection strings
        /// </summary>
        public ConnectionStrings ConnectionStrings { get; set; }
    }
}
