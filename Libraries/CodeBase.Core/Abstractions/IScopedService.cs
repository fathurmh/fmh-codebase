﻿namespace CodeBase.Core.Abstractions
{
    /// <summary>
    /// Objects which derived from this interface will be registered as scoped service in dependency injection container
    /// </summary>
    public interface IScopedService
    {
    }
}
