using System;
using CodeBase.Core.Abstractions;

namespace CodeBase.Core.Helpers.Abstractions
{
    public interface IValidationHelpers : ISingletonService
    {
        void ThrowIfNull(string parameter, string parameterName);
        void ThrowIfNull(object parameter, string parameterName);
        void ThrowIfNull<T>(T parameter, string parameterName);
    }
}
