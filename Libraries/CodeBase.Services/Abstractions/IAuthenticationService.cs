﻿using System.Threading.Tasks;
using CodeBase.Core.Abstractions;
using CodeBase.Services.Models;

namespace CodeBase.Services.Abstractions
{
    /// <summary>
    /// Represents an authentication service
    /// </summary>
    public interface IAuthenticationService : ITransientService
    {
        /// <summary>
        /// Represents a login authentication
        /// </summary>
        /// <param name="username">Provided username</param>
        /// <param name="password">Provided password</param>
        /// <returns></returns>
        Task<Authentication> Login(string username, string password);
    }
}
