﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CodeBase.Core.Data.Abstractions;
using CodeBase.Core.Helpers.Abstractions;
using CodeBase.Core.Providers;
using CodeBase.Core.Providers.Abstractions;
using CodeBase.Repositories.Entities;
using CodeBase.Repositories.Extensions;
using CodeBase.Services.Abstractions;
using CodeBase.Services.Models;
using Microsoft.IdentityModel.Tokens;

namespace CodeBase.Services.Implementations
{
    /// <summary>
    /// Represents an authentication service
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenProvider _tokenProvider;
        private readonly IValidationHelpers _validation;

        public AuthenticationService(IRepository<User> userRepository,
            IPasswordHasher passwordHasher,
            ITokenProvider tokenProvider,
            IValidationHelpers validation)
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _tokenProvider = tokenProvider;
            _validation = validation;
        }

        /// <summary>
        /// Represents a login authentication
        /// </summary>
        /// <param name="username">Provided username</param>
        /// <param name="password">Provided password</param>
        /// <returns></returns>
        public async Task<Authentication> Login(string username, string password)
        {
            _validation.ThrowIfNull(username, nameof(username));
            _validation.ThrowIfNull(password, nameof(password));

            User user = (await _userRepository.SelectAsync(prop => prop.Username == username, $"{nameof(User.UserRoles)}.{nameof(UserRole.Role)}")).Single();
            _validation.ThrowIfNull(user, nameof(user));

            IEnumerable<Role> roles = user.UserRoles.Select(prop => prop.Role);
            string[] roleNames = roles.Count() == 0 ? new string[] { "Guest" } : roles.Select(prop => prop.RoleName).ToArray();

            Authentication authentication = new Authentication();
            PasswordVerificationStatus verificationStatus = _passwordHasher.VerifyHashedPassword(user.Password, password);
            if (verificationStatus == PasswordVerificationStatus.Failed)
            {
                throw new ArgumentNullException("Username and password doesn't match.");
            }
            else if (verificationStatus == PasswordVerificationStatus.SuccessRehashNeeded)
            {
                string hashedPassword = _passwordHasher.HashPassword(user.Password);
                user.Password = hashedPassword;
                user.ModifiedBy = user.Username;
                user.ModifiedTime = DateTime.Now;
                _userRepository.Update(user);
            }

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username)
            };
            claims.AddRange(roleNames.Select(prop => new Claim(ClaimTypes.Role, prop)).ToList());

            authentication.Username = user.Username;
            authentication.Token = _tokenProvider.GenerateToken(claims.ToArray());
            authentication.Roles = claims.Where(prop => prop.Type == ClaimTypes.Role).Select(prop => prop.Value);
            authentication.TokenExpires = _tokenProvider.Expires;

            return authentication;
        }
    }
}
