﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeBase.Core.Abstractions;
using CodeBase.Core.Data.Abstractions;
using CodeBase.Repositories.Entities;
using CodeBase.Repositories.Extensions;
using CodeBase.Services.Abstractions;

namespace CodeBase.Services.Implementations
{
    /// <summary>
    /// Represents an user service
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Get list of users
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userRepository.SelectAsync();
        }

        /// <summary>
        /// Gets an user
        /// </summary>
        /// <param name="id">User identifier</param>
        /// <returns></returns>
        public async Task<User> GetUser(int id)
        {
            return await _userRepository.SelectAsync(id);
        }

        /// <summary>
        /// Creates an user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task CreateUser(User user)
        {
            await _userRepository.InsertAsync(user);
        }

        /// <summary>
        /// Updates an user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task UpdateUser(User user)
        {
            await _userRepository.UpdateAsync(user);
        }

        /// <summary>
        /// Deletes an user
        /// </summary>
        /// <param name="id">User identifier</param>
        /// <returns></returns>
        public async Task DeleteUser(int id)
        {
            User deletedUser = await _userRepository.SelectAsync(id);
            await _userRepository.DeleteAsync(deletedUser);
        }
    }
}
