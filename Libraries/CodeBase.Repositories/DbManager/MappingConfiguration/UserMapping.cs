﻿using CodeBase.Core.Data;
using CodeBase.Repositories.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeBase.Repositories.DbManager.MappingConfiguration
{
    /// <summary>
    /// Represents a user mapping configuration
    /// </summary>
    public sealed class UserMapping : EntityTypeConfiguration<User>
    {
        /// <summary>
        /// Add user post configuration
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        protected override void PostConfigure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(user => new { user.Username, user.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(User), nameof(User.Username), nameof(User.DeletedTime)))
                .IsUnique();
        }

        /// <summary>
        /// Configures the user entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));

            builder.Property(user => user.Username)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(user => user.Password)
                .IsRequired()
                .HasMaxLength(400);

            builder.HasMany(user => user.UserRoles)
                .WithOne()
                .HasForeignKey(userRole => userRole.UserId);

            base.Configure(builder);
        }
    }
}
