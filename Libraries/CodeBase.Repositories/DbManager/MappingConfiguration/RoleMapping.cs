﻿using CodeBase.Core.Data;
using CodeBase.Repositories.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeBase.Repositories.DbManager.MappingConfiguration
{
    /// <summary>
    /// Represents a role mapping configuration
    /// </summary>
    public sealed class RoleMapping : EntityTypeConfiguration<Role>
    {
        /// <summary>
        /// Add role post configuration
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        protected override void PostConfigure(EntityTypeBuilder<Role> builder)
        {
            builder.HasIndex(role => new { role.RoleCode, role.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(Role), nameof(Role.RoleCode), nameof(Role.DeletedTime)))
                .IsUnique();

            builder.HasIndex(role => new { role.RoleName, role.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(Role), nameof(Role.RoleName), nameof(Role.DeletedTime)))
                .IsUnique();
        }

        /// <summary>
        /// Configures the role entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable(nameof(Role));

            builder.Property(role => role.RoleCode)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(role => role.RoleName)
                .IsRequired()
                .HasMaxLength(400);

            builder.Property(role => role.RoleDescription)
                .IsRequired()
                .HasMaxLength(400);

            builder.HasMany(role => role.UserRoles)
                .WithOne()
                .HasForeignKey(userRole => userRole.RoleId);

            base.Configure(builder);
        }
    }
}
