﻿using CodeBase.Core.Data;
using CodeBase.Repositories.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeBase.Repositories.DbManager.MappingConfiguration
{
    /// <summary>
    /// Represents a role mapping configuration
    /// </summary>
    public sealed class UserRoleMapping : EntityTypeConfiguration<UserRole>
    {
        /// <summary>
        /// Add role post configuration
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        protected override void PostConfigure(EntityTypeBuilder<UserRole> builder)
        {
            builder.HasIndex(userRole => userRole.UserId)
                .HasName(FormatName(IndexPrefix, nameof(UserRole), nameof(UserRole.UserId)));

            builder.HasIndex(userRole => userRole.RoleId)
                .HasName(FormatName(IndexPrefix, nameof(UserRole), nameof(UserRole.RoleId)));
                
            builder.HasIndex(userRole => new { userRole.UserId, userRole.RoleId, userRole.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(UserRole), nameof(UserRole.UserId), nameof(UserRole.RoleId)))
                .IsUnique();
        }

        /// <summary>
        /// Configures the role entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable(nameof(UserRole));

            builder.HasOne(userRole => userRole.User)
                .WithMany(user => user.UserRoles)
                .HasForeignKey(userRole => userRole.UserId);

            builder.HasOne(userRole => userRole.Role)
                .WithMany(role => role.UserRoles)
                .HasForeignKey(userRole => userRole.RoleId);

            base.Configure(builder);
        }
    }
}
