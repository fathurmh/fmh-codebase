﻿using CodeBase.Core.Data;
using CodeBase.Repositories.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeBase.Repositories.DbManager.MappingConfiguration
{
    /// <summary>
    /// Represents a department mapping configuration
    /// </summary>
    public sealed class DepartmentMapping : EntityTypeConfiguration<Department>
    {
        /// <summary>
        /// Add department post configuration
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        protected override void PostConfigure(EntityTypeBuilder<Department> builder)
        {
            builder.HasIndex(department => new { department.DepartmentCode, department.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(Department), nameof(Department.DepartmentCode), nameof(Department.DeletedTime)))
                .IsUnique();

            builder.HasIndex(department => new { department.DepartmentName, department.DeletedTime })
                .HasName(FormatName(UniqueIndexPrefix, nameof(Department), nameof(Department.DepartmentName), nameof(Department.DeletedTime)))
                .IsUnique();
        }

        /// <summary>
        /// Configures the department entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.ToTable(nameof(Department));

            builder.Property(department => department.DepartmentCode)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(department => department.DepartmentName)
                .IsRequired()
                .HasMaxLength(400);

            builder.Property(department => department.DepartmentDescription)
                .IsRequired()
                .HasMaxLength(400);

            base.Configure(builder);
        }
    }
}
