# FMH ASP.NET Core 2.2 Code Base

[![build status](https://gitlab.com/fathurmh/fmh-codebase/badges/master/pipeline.svg)](https://gitlab.com/fathurmh/fmh-codebase/commits/master) [![made-with-love](https://img.shields.io/badge/made%20with-love-ff69b4.svg)](https://gitlab.com/fathurmh/fmh-codebase/)

Building Web API with ASP.NET Core 2.2

## Installing

### Clone this repository

```sh
git clone https://gitlab.com/fathurmh/fmh-codebase.git
cd fmh-codebase
```

### Create database migration

```sh
dotnet ef migrations add InitialCreate
dotnet ef database update
```

or

```sh
dotnet ef migrations add InitialCreate --project .\Presentation\CodeBase.Web.Api
dotnet ef migrations script --project .\Presentation\CodeBase.Web.Api --output .\Database\script.txt --idempotent
```

### Run application

```sh
dotnet restore
dotnet build
dotnet run
```

### Example API Response output

```json
{
    "Version": "1.0",
    "StatusCode": 200,
    "IsSuccess": true,
    "Message": [
        "Request successful."
    ],
    "Result": [
        {
            "id": 1,
            "ApplicationName": "Sample Application Name",
            "ApplicationDescription": "Sample Application Description"
        }
    ]
}
```
