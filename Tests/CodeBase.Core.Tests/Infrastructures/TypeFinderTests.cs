using System.Linq;
using CodeBase.Core.Helpers;
using CodeBase.Core.Infrastructures;
using CodeBase.Core.Providers;
using CodeBase.Core.Providers.Abstractions;
using CodeBase.Tests.Fakes;
using Microsoft.AspNetCore.Hosting;
using Moq;
using Xunit;

namespace CodeBase.Core.Tests.Infrastructures
{
    public class TypeFinderTests
    {
        [Fact]
        public void Benchmark_Findings()
        {
            var hostingEnvironment = new Mock<IHostingEnvironment>();
            hostingEnvironment.Setup(x => x.ContentRootPath).Returns(System.Reflection.Assembly.GetExecutingAssembly().Location);
            hostingEnvironment.Setup(x => x.WebRootPath).Returns(System.IO.Directory.GetCurrentDirectory());
            ICoreFileProvider coreFileProvider = new CoreFileProvider(hostingEnvironment.Object);
            var finder = new AppDomainTypeFinder(coreFileProvider);
            var type = finder.FindClassesOfType<ITransientService>().ToList();

            Assert.Equal(4, type.Count);
            Assert.True(typeof(ITransientService).IsAssignableFrom(type.FirstOrDefault()));
        }
    }
}
