using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using CodeBase.Core.Exceptions;
using CodeBase.Core.Filters;
using CodeBase.Core.Infrastructures;
using CodeBase.Core.Localization.Resources;
using CodeBase.Tests;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CodeBase.Core.Tests.Filters
{
    public class ApiExceptionFilterAttributeTests : TestsBase
    {
        [Theory]
        [MemberData(nameof(ExceptionData))]
        public async Task Should_Filter_Exception(Exception exception, string exceptionMessage, int httpStatusCode)
        {
            var exceptionContext = GetExceptionContext(exception);
            var stringLocalizer = GetMockStringLocalizer<ApiResponseMessage>(ApiResponseMessage.Exception, ApiResponseMessage.Exception);
            var apiExceptionFilter = new ApiExceptionFilterAttribute(stringLocalizer.Object);

            await apiExceptionFilter.OnExceptionAsync(exceptionContext);
            var result = (exceptionContext.Result as JsonResult).Value as ApiResponse<object>;

            Assert.Equal(httpStatusCode, result.StatusCode);
            Assert.Equal(exceptionMessage, result.ResponseException.ExceptionMessage);
        }
        
        public static IEnumerable<object[]> ExceptionData()
        {
            return new List<object[]>
            {
                new object[] { new Exception("Exception Test"), "Exception Test", (int)HttpStatusCode.InternalServerError },
                new object[] { new CoreException("Core Exception Test"), "Core Exception Test", (int)HttpStatusCode.OK },
                new object[] { new ApiException("Api Exception Test"), "Api Exception Test", (int)HttpStatusCode.InternalServerError },
            };
        }
    }
}
