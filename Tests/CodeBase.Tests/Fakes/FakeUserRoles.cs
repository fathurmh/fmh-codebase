using System;
using System.Collections.Generic;
using CodeBase.Repositories.Entities;

namespace CodeBase.Tests.Fakes
{
    public class FakeUserRoles : List<UserRole>
    {
        public FakeUserRoles()
        {
            Add(new UserRole() {Id = 1, UserId = 1, RoleId = 1});
            Add(new UserRole() {Id = 2, UserId = 1, RoleId = 2});
            Add(new UserRole() {Id = 3, UserId = 1, RoleId = 3});
            Add(new UserRole() {Id = 4, UserId = 1, RoleId = 4});
            Add(new UserRole() {Id = 5, UserId = 1, RoleId = 5});
            
            Add(new UserRole() {Id = 6, UserId = 2, RoleId = 1});
            Add(new UserRole() {Id = 7, UserId = 2, RoleId = 2});
            Add(new UserRole() {Id = 8, UserId = 2, RoleId = 3});
            Add(new UserRole() {Id = 9, UserId = 2, RoleId = 4});
            
            Add(new UserRole() {Id = 10, UserId = 3, RoleId = 1});
            Add(new UserRole() {Id = 11, UserId = 3, RoleId = 2});
            Add(new UserRole() {Id = 12, UserId = 3, RoleId = 3});
            
            Add(new UserRole() {Id = 13, UserId = 4, RoleId = 1});
            Add(new UserRole() {Id = 14, UserId = 4, RoleId = 2});
        }
    }
}
