using System;
using System.Collections.Generic;
using CodeBase.Repositories.Entities;

namespace CodeBase.Tests.Fakes
{
    public class FakeUsers : List<User>
    {
        public FakeUsers()
        {
            Add(new User() {Id = 1, Username = "Username 1", Password = "Password 1" });
            Add(new User() {Id = 2, Username = "Username 2", Password = "Password 2" });
            Add(new User() {Id = 3, Username = "Username 3", Password = "Password 3" });
            Add(new User() {Id = 4, Username = "Username 4", Password = "Password 4" });
            Add(new User() {Id = 5, Username = "Username 5", Password = "Password 5" });
        }
    }
}
