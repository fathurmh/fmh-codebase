using System;
using System.Collections.Generic;
using CodeBase.Repositories.Entities;

namespace CodeBase.Tests.Fakes
{
    public class FakeRoles : List<Role>
    {
        public FakeRoles()
        {
            Add(new Role() {Id = 1, RoleCode = "Role Code 1", RoleName = "Role Name 1", RoleDescription = "Role Description 1"});
            Add(new Role() {Id = 2, RoleCode = "Role Code 2", RoleName = "Role Name 2", RoleDescription = "Role Description 2"});
            Add(new Role() {Id = 3, RoleCode = "Role Code 3", RoleName = "Role Name 3", RoleDescription = "Role Description 3"});
            Add(new Role() {Id = 4, RoleCode = "Role Code 4", RoleName = "Role Name 4", RoleDescription = "Role Description 4"});
            Add(new Role() {Id = 5, RoleCode = "Role Code 5", RoleName = "Role Name 5", RoleDescription = "Role Description 5"});
        }
    }
}
